const { parse } = require("pg-connection-string");

let ssl;
if (process.env.NODE_ENV !== "development") {
  ssl = {
    rejectUnauthorized: false,
  };
}

module.exports = {
  client: "postgresql",
  connection: {
    ...parse(process.env.DATABASE_URL),
    ssl,
  },
  pool: {
    min: 1,
    max: 10,
  },
};
